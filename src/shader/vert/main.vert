#version 430 core

// inputs
layout(location = 0) in vec2 vertexPosition;

// outputs
out vec2 uv;

// uniforms
uniform mat4 projection;

  
void main(){

  // calculate the position
  gl_Position =  projection * vec4(vertexPosition.x, vertexPosition.y, -1, 1);

  // pass on the uv coordinates
  uv = vertexPosition + vec2(0.5f);
}
